<?php
    class Persona{
        //atributos
        protected string $nombre;
        protected string $altura;
        protected string $nacionalidad;
        protected int $edad;
        //constructor
        public function __construct(string $nombre, string $altura, string $nacionalidad)
        {
            $this->nombre = $nombre;
            $this->altura = $altura;
            $this->nacionalidad = $nacionalidad;
        }
        //metodos getter y setter
        public function getNombre(): string
        {
            return $this->nombre;
        }

        public function setNombre(string $nombre): void
        {
            $this->nombre = $nombre;
        }

        public function setAltura(string $altura): void
        {
            $this->altura = $altura;
        }

        public function setEdad(int $edad): void
        {
            $this->edad = $edad;
        }

    }

    $Inma = new Persona("Inma","160","española");

    $Inma->setEdad(36);

    print_r($Inma);
