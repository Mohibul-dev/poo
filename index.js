// console.log("Hola mundo");

const persona = {
    //atributos imitacion
    nombre: "", //obligatorio
    altura: "",//obligatorio
    nacionalidad: "",//obligatorio
    edad: "", //opcional

    constructor(nombre,altura,nacionalidad) { //constructor
        this.nombre = nombre;
        this.altura = altura;
        this.nacionalidad = nacionalidad;
    },

    getNombre: function(){ //metodo getter --> sirve para obtener un atributo
        return this.nombre;
    },

    setNombre: function(nombre){ //metodo setter --> para asignar un nuevo valor a un atributo
        this.nombre = nombre;
    },

    setEdad: function(edad){ //metodo setter --> para asignar un nuevo valor a un atributo
        this.edad = edad;
    },

}

class Persona {
    //atributo
    nombre = "";
    altura = "";
    nacionalidad = "";
    edad = null;
    //constructor
    constructor(nombre,altura,nacionalidad) { //constructor
        this.nombre = nombre;
        this.altura = altura;
        this.nacionalidad = nacionalidad;
    };
    //metodos getter y setter
    getNombre(){ //metodo getter --> sirve para obtener un atributo
        return this.nombre;
    };

    setNombre(nombre){ //metodo setter -->
        this.nombre = nombre;
    };

    setEdad(edad){
        this.edad = edad;
    };

    setAltura(altura){
        this.altura = altura;
    }

}

//prueba con objeto -->

persona.constructor("ana","170","español");

// console.log("linea 61-->",persona);

// console.log("linea 30-->",persona.getNombre()); //devuelve pepe

persona.setNombre("juan");

// console.log("linea 38-->",persona); //devuelve pepe2


//prueba con clase -->


const personaClass = new Persona("david","199","español"); //instanciamos la clase, lo creamos y lo dejamos en memoria

// console.log("linea 72-->",personaClass);

personaClass.setEdad(18);

// console.log("linea 77-->",personaClass);

const myTeam = [];

const Inma = new Persona("Inma","160","española");

Inma.setEdad(36)

const Trigo = new Persona("Trigo","181","español");

myTeam.push(Inma);
myTeam.push(Trigo);

console.log("linea 90 -->",myTeam);

const javi = new Persona();

console.log("linea 111-->", javi);

javi.setAltura("169");
javi.setEdad("25");

console.log("linea 116-->", javi);


// porque yo suelo poner constructor vacio?
